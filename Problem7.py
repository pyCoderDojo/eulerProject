import logging
import Problem3

# n legt gewünschte Menge an Priemzahlen fest
def solve(n):
    # variable wird alle Priemzahlen enthalten
    primes = []
    # Startwert
    current = 0
    while(len(primes)<n):
        if Problem3.isPrime(current):
            primes.append(current)
        current += 1

    logging.info(primes[-1])


if __name__ == "__main__":
    FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT)
    solve(10001)
