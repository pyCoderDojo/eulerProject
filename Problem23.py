import math

setnaturals = set(range(1, 28123 + 1))


def isAbundent(x):
    divsum = 1
    divs = range(2, math.floor(math.sqrt(x))+1)
    for psbldiv in divs:
        if x % psbldiv == 0:
            div2 = int(x // psbldiv)
            if (div2 == psbldiv):
                divsum += (div2)
            else:
                divsum += (div2 + psbldiv)

        if divsum > x:
            return True

    return False


buns = set()
for i in range(12, 28123 + 1):
    if isAbundent(i):
        buns.add(i)
print(f"abundents: %s" % buns)

setofsumsofbuns = set()
for e1 in buns:
    for e2 in buns:
        sum = e1 + e2
        if sum <= 28123:
            setofsumsofbuns.add(sum)
        else:
            break
print(f"setofsumsofbuns: %s" % setofsumsofbuns)

resultNaturals = setnaturals - setofsumsofbuns
print(f"resultNaturals: %s" % resultNaturals)

summme = 0
for i in resultNaturals:
    summme += i
print(summme)
