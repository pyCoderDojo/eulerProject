import logging
import math


primeCache = []

def isPrime(num, check=None):
    global primeCache
    if num in primeCache:
        logging.debug("cache hit")
        return True
    elif check == None:
        return isPrime(num, int(math.sqrt(num)))
    elif num <= 1:
        return False
    elif check == 1:
        primeCache.append(num)
        return True
    else:
        if num % check == 0:
            return False
        else:
            return isPrime(num, check - 1)


def getAllPrimesBelow(num):
    primes = []
    for i in range(2, num):
        if isPrime(i):
            primes.append(i)
    return primes


def getAllPrimesBelow2(num):
    primes = list(range(2, num))
    n = int(math.ceil(math.sqrt(num)))
    for a in range(2, n + 1):
        for b in range(2, n + 1):
            c = a * b
            if c > 2 and c < num and c in primes:
                primes.remove(c)
    return primes


def maxPrimeFact(num):
    for i in range(int(math.sqrt(num)) + 1, 0, -1):
        if i <= 2:
            return None
        elif num % i == 0 and isPrime(i):
            return i


def solve():
    # num = 600851475143
    num = 1471475143
    # num = 13195
    maxPF = maxPrimeFact(num)
    logging.info(f"max prime fact: {maxPF}")


if __name__ == "__main__":
    FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT)
    solve()

