import datetime

# 0 == Monday
# 6 == Sunday
firstWeekDay = 0
day = 1
month = 1
year = 1900


# based on month and yeat as an int returns number of days per month
def daysOfMonth(m, y):
    if m == 2:
        if y % 4 == 0 and (not y % 100 == 0 or y % 400 == 0):
            return 29
        else:
            return 28
    elif m in [1, 3, 5, 7, 8, 10, 12]:
        return 31
    else:
        return 30


while year <= 2000:
    dpm = daysOfMonth(month, year)
    mondays = int((dpm - firstWeekDay) / 7)
    print(f"(first day of month: %d) mondays %d in month %d and year %d" % (firstWeekDay, mondays, month, year))
    firstWeekDay = int((dpm - firstWeekDay) % 7)
    month += 1
    if month > 12:
        month = 1
        year += 1

print("done")
