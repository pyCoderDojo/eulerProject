import logging


def solve():
    threeD = list(range(100, 999+1))
    numbers = []
    for i in threeD:
        iStr = str(i)
        palin = "{}{}".format(iStr, iStr[::-1])
        numbers.append(int(palin))

    numbers = list(reversed(numbers))
    logging.info(numbers)
    didIt = False
    for i in numbers:
        divisors = list(range(999, 100 - 1, -1))
        for j in divisors:
            if i % j == 0  and int(i/j) <= 999:
                logging.info("{} * {} = {}".format(j, int(i/j), i))
                didIt = True
                break
        if didIt:
            break


if __name__ == "__main__":
    FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT)
    solve()
