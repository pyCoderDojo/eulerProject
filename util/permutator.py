

class Permutator:
    """ this class helps to permute a elements of a given alphabet

        solves Euler Project Problem 24
        https://www.geeksforgeeks.org/write-a-c-program-to-print-all-permutations-of-a-given-string/
    """

    def __s2List(self, aString):
        """ internal helper that converts a python string to a python list

        Parameters
        ----------
        aString : String
            given string will be converted i a Python list, e.g. "012" -> ['0', '1', '2']

        Returns
        -------
        list
            given String as list
        """
        return list(aString)

    def __doPermute(self, alphabet, perms, current=''):
        """ this internal function is doing at the end all the work

        Parameters
        ----------
        alphabet : String
            current alphabet, which will be permuted
        perms :  List
            list of all permutation
        current : String
            current permutation

        Returns
        -------
        list
            permutations of given alphabet as a list
        """
        if len(alphabet) == 1:
            perms.append(current + alphabet[0])
        else:
            for c in alphabet:
                c2 = alphabet.copy()
                c2.remove(c)
                self.__doPermute(c2, perms, current + c)

        return perms

    def permute(self, alphabet):
        """ generates all permutations of a given alphabet, e.g. "012" ->  ['012', '021', '102', '120', '201', '210']

        Parameters
        ----------
        alphabet : String
            alphabet, which will be permuted

        Returns
        -------
        list
            permutations of given alphabet as a list
        """
        perm = self.__doPermute(self.__s2List(alphabet), list())
        perm.sort()
        return perm

