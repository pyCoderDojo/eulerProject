import logging
import math


def getDivisors(n):
    div = [1]
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            div.append(i)
            div.append(int(n // i))
    div.sort()
    return div


def solve(n):
    c = 0
    tn = 0

    while True:
        c += 1
        tn += c
        divs = getDivisors(tn)

        if len(divs) >= n:
            break
    return (tn, divs)


if __name__ == "__main__":
    FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT)
    dreickszahl, teiler = solve(500)
    logging.info(f"%d: %s" % (dreickszahl, teiler))
