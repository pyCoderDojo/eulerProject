import logging

def collatz(n):
    if n == 1:
        return [n]

    r = [n]
    if n % 2 == 0:
        r.extend(collatz(n // 2))
    else:
        r.extend(collatz((3 * n) + 1))

    return r


def solve(n):
    col = []
    for i in range(1, n):
        c = collatz(i)
        if len(c) > len(col):
            col = c

    return col

if __name__ == "__main__":
    FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT)
    maxCollatz = solve(1000000)
    logging.info(maxCollatz)
