# Euler Problem 24
# https://www.geeksforgeeks.org/write-a-c-program-to-print-all-permutations-of-a-given-string/

from util.permutator import Permutator

p = Permutator()

r = p.permute('0123')
print(r)

r = p.permute('abcd')
print(r)

r = p.permute('0123456789')
print(r[1000000-1])
