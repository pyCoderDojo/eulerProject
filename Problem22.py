# Euler Problem 22

def readNames():
    # so öffnen wie die Datei names.txt, die die Namen alle enthält
    file = open("names.txt", 'r')
    names = ""
    # in der Schleife lesen wir die gesamte Datei zeilweise ein
    for line in file:
        # nur zur Sicherheit entfernen wir die Steuer zeichen "new line" (\n, Zeilenvorschub) und "carriage return" (\r, Wagenrücklauf)
        names += line.replace("\n", "").replace("\r", "")

    # die enthaltenen '"' Zeichen stören uns nur, also weg damit
    names = names.replace("\"", "")

    # diese Funktion macht aus einem String eine Liste in dem sie am Trennzeichen (hier ',') den String zertrennt
    namesList = names.split(',')
    # nun noch die Liste sortieren
    namesList.sort()
    file.close()
    return namesList

def getAlphabet():
    # hier folgt nun ein Weg eine Zeichenkette mit allen Großbuchstaben des Alphabets aufzubauen
    # der Buchstabe A wird im ASCII Alphabet durch die Zahl 65 repräsentiert
    # mehr zum ASCII Alphabet hier: http://www.asciitable.com
    alphabet = ""
    asciiA = 65
    for i in range(asciiA, asciiA+25+1):
        alphabet += chr(i)
    return alphabet

# sum of the alphabetic ordered letters
def sumAlpha(name, alpha):
    sum = 0
    for letter in name:
        sumand = alpha.find(letter)+1
        sum += sumand
    return sum



# goes throw dictonary and
# gives every name into alpha
# calculates the sum of prdct
# like  sum = (alphabetic number of letter +...) *index of name

def solve(nl, alpha):
    total = 0
    for name in nl:
        sum = sumAlpha(name, alpha)
        prdct = sum * (nl.index(name)+1)
        total += prdct
    return total

print(solve(readNames(), getAlphabet()))
