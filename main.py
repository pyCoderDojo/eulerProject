import logging
import sys
sys.setrecursionlimit(10**4)

FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)

import Problem1
import Problem2
import Problem3
import Problem4
import Problem5
import Problem6
import Problem7
import Problem8
import Problem9
import Problem10
import Problem11
import Problem11a

# Problem1.solve()
# Problem2.solve()
# Problem3.solve()
# Problem4.solve()
# Problem5.solve()
# Problem6.solve()
#Problem7.solve(10001)
# Problem8.solve(13)
# Problem9.solve(1000)
# Problem10.solve(10)
# Problem10.solve(2*1000*1000)
# Problem11.solve(Problem11.matrix)
Problem11a.solve(Problem11a.getMatrix(), 4)
Problem11a.solve(Problem11a.getRandomMatrix(10,15), 5)
