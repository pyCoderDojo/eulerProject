import logging
import Problem3


def find(n):
    logging.info("started")
    allPrimes = Problem3.getAllPrimesBelow2(n)
    logging.info("finished")
    return sum(allPrimes)


def solve(n):
    FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT)
    sum = find(n)
    logging.info(sum)


if __name__ == "__main__":
    FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT)
    solve(2*1000)

