import math


def findabundents2(x):
    divsum = 1
    for psbldiv in range(2, math.floor(math.sqrt(x)) + 1):
        if x % psbldiv == 0:
            div2 = int(x / psbldiv)
            divsum += div2 + psbldiv

    if divsum < x:
        return "deficient"
    elif divsum > x:
        return "abundant"
    else:
        return "perfect"


deficient = []
abundant = []
perfect = []

for i in range(2, 10000 + 1):
    res = findabundents2(i)
    if res == "perfect":
        print(f"%d : %s" % (i, res))


def findabundents(x):
    divsum = 1
    for psbldiv in range(2, math.floor(math.sqrt(x)) + 1):
        if x % psbldiv == 0:
            div2 = x / psbldiv
            divsum += div2 + psbldiv

        if divsum > x:
            return True
    return False
    # print(x, "<=" ,divsum)
    # prints out nonabundants


naturalnums = []
for i in range(1, 28123):
    naturalnums.append(i)

setnaturals = set(naturalnums)

buns = []
for i in range(12, 28123):
    ifabundant = findabundents(i)
    # print(abun)
    if ifabundant:
        buns.append(i)
# print(buns)

listofsumsofbuns = []
for e1 in buns:
    for e2 in buns:
        sum = e1 + e2
        if sum > 28123:
            break
        listofsumsofbuns.append(sum)


setofsumsofbuns = set(listofsumsofbuns)
print(setofsumsofbuns)


result = setofsumsofbuns.symmetric_difference(setnaturals)
print(result)
