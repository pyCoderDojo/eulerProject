import logging


def sumIt(a, b, m=1000, c=0):
    logging.debug(f"c : {c}")
    if c >= m:
        return 0
    elif (c % a == 0) or (c % b == 0):
        return c + sumIt(a, b, m, c + 1)
    else:
        return sumIt(a, b, m, c + 1)


def solve():
    sum = sumIt(3, 5)
    logging.info(f"Summe: {sum}")


if __name__ == "__main__":
    FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT)
    solve()
