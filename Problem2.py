import logging

def fibo(l, a=1, b=2):
    if (l < b):
        return [a]
    else:
        return [a] + fibo(l, b, b+a)

def evenFibo(l):
    return list(filter(lambda x: x % 2 == 0, fibo(l)))

def solve():
    fiboLimit = 4000000
    fib = fibo(fiboLimit)
    fibS = sum(fib)
    fibEven = evenFibo(fiboLimit)
    fibEvenS = sum(fibEven)

    logging.info(f"fib: {fib}")
    logging.info(f"fibS: {fibS}")
    logging.info(f"fibEven: {fibEven}")
    logging.info(f"fibEvenS: {fibEvenS}")


if __name__ == "__main__":
    FORMAT = '%(asctime)-20s [%(levelname)s]: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT)
    solve()


